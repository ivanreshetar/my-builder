# Иван Решетар - учебная сборка LoftSchool 

> Сборка работает на gulp версии 4.0. 

#### Для начала работы

1. ```clone this repo```
2. ```cd path/to/...```
3. ```npm install gulpjs/gulp-cli -g```  
> Установка последней версии Gulp CLI tools глобально (подробнее - [GitHub](https://github.com/gulpjs/gulp/blob/4.0/docs/getting-started.md) )

4. ```npm install```
5. ```npm i -D gulp.spritesmith```
6. ```run gulp``` 

#### Сделано:

- создана папка /source/sprites/ и в неё загружены 
картинки форматов png, gif, svg
- в папке /gulp/tasks/ созданы файлы: 
    1) copy.font.js - добавлен модуль копирования шрифтов
    2) sprite.png.js - добавлен модуль создания спрайтов из 
png-файлов
    3) sprite.gif.js - добавлен модуль создания спрайтов из 
                gif-файлов
- в файл /gulp/tasks/sprite.svg.js - внесены изменения 
для создания svg-спрайтов
- в файл /gulp/paths/tasks.js добавлены пути к новым таскам
- добавлены таски в дефолтный модуль
- установлен пакет gulp.spritesmith
- отредактирован файл README.md 

